import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cuerpo',
  templateUrl: './cuerpo.component.html',
  styleUrls: ['./cuerpo.component.css']
})
export class CuerpoComponent implements OnInit {

  constructor() { }
  cargar = false;

  ejecutar(): void{
      this.cargar = true;
  
      setTimeout(() => {
        this.cargar = false;
      }, 3000);
    }
  enviar=false;
    enviando(): void{
      this.enviar = true;
  
      setTimeout(() => {
        this.enviar = false;
      }, 3000);
    }
  ngOnInit(): void {
  }

}
